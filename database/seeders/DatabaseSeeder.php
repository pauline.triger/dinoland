<?php

namespace Database\Seeders;

use App\Models\Adresse;
use App\Models\Caracteristique;
use App\Models\CategorieProduit;
use App\Models\Client;
use App\Models\Climat;
use App\Models\Commande;
use App\Models\Dinosaure;
use App\Models\Enclos;
use App\Models\Environnement;
use App\Models\Espece;
use App\Models\Nourriture;
use App\Models\Pays;
use App\Models\Personnel;
use App\Models\Produit;
use App\Models\StatutCommande;
use App\Models\ModeLivraison;
use App\Models\Taxe;
use App\Models\TypeEnclos;
use App\Models\TypePersonnel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        ModeLivraison::factory(2)->create();
        StatutCommande::factory(5)->create();
        Espece::factory(10)->create();
        Caracteristique::factory(10)->create();
        Nourriture::factory(5)->create();
        TypeEnclos::factory(3)->create();
        Climat::factory(5)->create();
        Environnement::factory(5)->create();
        TypePersonnel::factory(10)->create();
        Pays::factory(1)->create();
        Taxe::factory(4)->create();
        CategorieProduit::factory(5)->create();

        Adresse::factory(20)
            ->make()
            ->each(function ($adresse) {
                $adresse
                    ->pays()->associate(Pays::inRandomOrder()->first())
                    ->save();
            });

        Personnel::factory(10)
            ->make()
            ->each(function ($personnel) {
                $personnel
                    ->typePersonnel()->associate(TypePersonnel::inRandomOrder()->first())
                    ->adresse()->associate(Adresse::inRandomOrder()->first())
                    ->save();
                });

        Enclos::factory(15)
            ->make()
            ->each(function ($enclos) {
                $enclos
                    ->climat()->associate(Climat::inRandomOrder()->first())
                    ->typeEnclos()->associate(TypeEnclos::inRandomOrder()->first())
                    ->save();
                $enclos
                    ->personnels()->attach(
                        Personnel::all()->random(3)->modelKeys()
                    );
                $enclos
                    ->environnements()->attach(
                        Environnement::inRandomOrder()->first(),
                        ['superficie' => mt_rand(4,10)]
                    );
            });

        Dinosaure::factory(30)
            ->make()
            ->each(function ($dinosaure) {
                $dinosaure
                    ->nourriture()->associate(Nourriture::inRandomOrder()->first())
                    ->espece()->associate(Espece::inRandomOrder()->first())
                    ->enclos()->associate(Enclos::inRandomOrder()->first())
                    ->save();
                $dinosaure
                    ->caracteristiques()->attach(
                        Caracteristique::all()->random(mt_rand(0,4))->modelKeys()
                    );
            });

        Produit::factory(15)
            ->make()
            ->each(function ($produit) {
                $produit
                    ->taxe()->associate(Taxe::inRandomOrder()->first())
                    ->categorie()->associate(CategorieProduit::inRandomOrder()->first())
                    ->save();
            });

        Client::factory(10)
            ->make()
            ->each(function ($client) {
                $adresse = Adresse::inRandomOrder()->first();
                $client
                    ->adresseFacturation()->associate($adresse)
                    ->adresseLivraison()->associate($adresse)
                    ->save();
                $client
                    ->produits()->attach(
                        Produit::all()->random(1,4)->modelKeys(),
                        ['quantite' => mt_rand(1,10)]
                    );
            });

        Commande::factory(20)
            ->make()
            ->each(function ($commande) {
                $commande
                    ->modeLivraison()->associate(ModeLivraison::inRandomOrder()->first())
                    ->statutCommande()->associate(StatutCommande::inRandomOrder()->first())
                    ->client()->associate(Client::inRandomOrder()->first())
                    ->save();
                $commande
                    ->produits()->attach(
                        Produit::all()->random(1,4)->modelKeys(),
                        ['quantite' => mt_rand(1,3), 'prixHT' => mt_rand(1000,10000)/100, 'taux' => mt_rand(1,100)/100]
                    );
                });
    }

    private function newDate() {
        $year = mt_rand(2000, 2022);
        $month = mt_rand(1, 12);
        $day = $month === (4||6||9||11) ? mt_rand(1, 30) : mt_rand(1, 31);

        $randomDate = new \DateTime($year.'-'.$month.'-'.$day.' 00:00:00');

        return $randomDate;
    }
}
