<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PersonnelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $pFirstName = $this->faker->firstName;
        $pLastName = $this->faker->lastName;

        return [
            'nom' => $pLastName,
            'prenom' => $pFirstName,
            'tel' => $this->faker->phoneNumber,
            'email' => Str::slug($pFirstName).'.'.Str::slug($pLastName).'@dinoland.com'
        ];
    }
}
