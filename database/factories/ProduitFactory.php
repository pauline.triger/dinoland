<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProduitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->word,
            'prix' => $this->faker->randomFloat(2, 0.01, 300),
            'quantite' => $this->faker->randomNumber(4),
            'image' => $this->faker->imageUrl(200, 200),
        ];
    }
}
