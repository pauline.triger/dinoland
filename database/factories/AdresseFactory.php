<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AdresseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'numero' => $this->faker->numberBetween(1,300),
            'rue' => $this->faker->streetName,
            'cp' => $this->faker->postcode,
            'ville' => $this->faker->city
        ];
    }
}
