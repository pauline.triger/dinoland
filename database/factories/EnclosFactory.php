<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class EnclosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => strtoupper($this->faker->randomLetter) . $this->faker->randomDigit,
            'superficie' => $this->faker->numberBetween(1,800),
        ];
    }
}
