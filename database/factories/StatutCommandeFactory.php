<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StatutCommandeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->randomElement(['validée', 'en traitement', 'livrée', 'annulée', 'en attente de confirmation'])
        ];
    }
}
