<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class DinosaureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->firstName,
            'taille' => $this->faker->randomFloat(2, 0.1,50),
            'poids' => $this->faker->numberBetween(1,100),
            'image' => $this->faker->imageUrl(200, 200),
        ];
    }
}
