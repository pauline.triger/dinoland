<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->float('prixTTC');
            $table->unsignedBigInteger('mode_livraison_id');
            $table->unsignedBigInteger('statut_commande_id');
            $table->unsignedBigInteger('client_id');
            $table->timestamps();
            $table->foreign('mode_livraison_id')->references('id')->on('mode_livraisons')->cascadeOnDelete();
            $table->foreign('statut_commande_id')->references('id')->on('statuts_commandes')->cascadeOnDelete();
            $table->foreign('client_id')->references('id')->on('clients')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commandes', function (Blueprint $table) {
            $table->dropForeign(['mode_livraison_id']);
            $table->dropForeign(['statut_commande_id']);
            $table->dropForeign(['client_id']);
        });
        Schema::dropIfExists('commandes');
    }
}
