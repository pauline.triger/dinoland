<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaracteristiqueDinoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caracteristique_dino', function (Blueprint $table) {
            $table->bigInteger('dino_id')->unsigned();
            $table->bigInteger('caracteristique_id')->unsigned();
            $table->timestamps();
            $table->foreign('dino_id')->references('id')->on('dinos')->onDelete('cascade');;
            $table->foreign('caracteristique_id')->references('id')->on('caracteristiques')->onDelete('cascade');;
            $table->primary(['dino_id', 'caracteristique_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('caracteristique_dino', function (Blueprint $table) {
            $table->dropForeign(['dino_id']);
            $table->dropForeign(['caracteristique_id']);
            $table->dropPrimary(['dino_id', 'caracteristique_id']);
        });
        Schema::dropIfExists('caracteristique_dino');
    }
}
