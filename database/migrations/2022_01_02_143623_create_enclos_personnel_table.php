<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnclosPersonnelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enclos_personnel', function (Blueprint $table) {
            $table->unsignedBigInteger('enclos_id');
            $table->unsignedBigInteger('personnel_id');
            $table->timestamps();
            $table->foreign('enclos_id')->references('id')->on('enclos')->onDelete('cascade');
            $table->foreign('personnel_id')->references('id')->on('personnels')->onDelete('cascade');
            $table->primary(['enclos_id', 'personnel_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enclos_personnel', function (Blueprint $table) {
            $table->dropForeign(['enclos_id']);
            $table->dropForeign(['personnel_id']);
            $table->dropPrimary(['enclos_id', 'personnel_id']);
        });

        Schema::dropIfExists('enclos_personnel');
    }
}
