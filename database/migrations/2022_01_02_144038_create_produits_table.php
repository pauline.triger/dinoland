<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->float('prix');
            $table->integer('quantite');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('taxe_id');
            $table->unsignedBigInteger('categorie_produit_id');
            $table->timestamps();
            $table->foreign('taxe_id')->references('id')->on('taxes')->cascadeOnDelete();
            $table->foreign('categorie_produit_id')->references('id')->on('categories_produits')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produits', function (Blueprint $table) {
            $table->dropConstrainedForeignId('taxe_id');
            $table->dropConstrainedForeignId('categorie_produit_id');
        });
        Schema::dropIfExists('produits');
    }
}
