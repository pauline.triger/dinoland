<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personnels', function (Blueprint $table) {
            $table->id();
            $table->string('prenom');
            $table->string('nom');
            $table->string('tel');
            $table->string('email');
            $table->unsignedBigInteger('adresse_id');
            $table->unsignedBigInteger('type_id');
            $table->timestamps();
            $table->foreign('adresse_id')->references('id')->on('adresses')->cascadeOnDelete();
            $table->foreign('type_id')->references('id')->on('types_personnels')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personnels', function (Blueprint $table) {
            $table->dropForeign(['adresse_id']);
            $table->dropForeign(['type_id']);
        });

        Schema::dropIfExists('personnels');
    }
}
