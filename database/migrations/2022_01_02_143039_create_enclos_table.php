<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnclosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enclos', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->float('superficie');
            $table->unsignedBigInteger('type_enclos_id');
            $table->unsignedBigInteger('climat_id');
            $table->timestamps();
            $table->foreign('type_enclos_id')->references('id')->on('types_enclos')->onDelete('cascade');
            $table->foreign('climat_id')->references('id')->on('climats')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enclos', function (Blueprint $table) {
            $table->dropForeign(['type_enclos_id']);
            $table->dropForeign(['climat_id']);
        });

        Schema::dropIfExists('enclos');
    }
}
