<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adresses', function (Blueprint $table) {
            $table->id();
            $table->integer('numero')->nullable();
            $table->string('rue');
            $table->string('cp');
            $table->string('ville');
            $table->unsignedBigInteger('pays_id');
            $table->timestamps();
            $table->foreign('pays_id')->references('id')->on('pays')->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adresses', function (Blueprint $table) {
            $table->dropForeign(['pays_id']);
        });
        Schema::dropIfExists('adresses');
    }
}
