<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDinosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dinos', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->float('taille');
            $table->float('poids');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('espece_id');
            $table->unsignedBigInteger('nourriture_id');
            $table->unsignedBigInteger('enclos_id');
            $table->timestamps();
            $table->foreign('espece_id')->references('id')->on('especes')->cascadeOnDelete();
            $table->foreign('nourriture_id')->references('id')->on('nourritures')->cascadeOnDelete();
            $table->foreign('enclos_id')->references('id')->on('enclos')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dinos', function (Blueprint $table) {
            $table->dropForeign(['espece_id']);
            $table->dropForeign(['nourriture_id']);
            $table->dropForeign(['enclos_id']);
        });
        Schema::dropIfExists('dinos');
    }
}
