<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('prenom');
            $table->string('nom');
            $table->string('tel');
            $table->unsignedBigInteger('adresse_livraison_id')->nullable();
            $table->unsignedBigInteger('adresse_facturation_id')->nullable();
            $table->timestamps();
            $table->foreign('adresse_livraison_id')->references('id')->on('adresses')->cascadeOnDelete();
            $table->foreign('adresse_facturation_id')->references('id')->on('adresses')->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign(['adresse_livraison_id']);
            $table->dropForeign(['adresse_facturation_id']);
        });
        Schema::dropIfExists('clients');
    }
}
