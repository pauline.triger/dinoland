<?php

use App\Http\Controllers\AdresseController;
use App\Http\Controllers\CaracteristiqueController;
use App\Http\Controllers\CategorieProduitController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ClimatController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\DinosaureController;
use App\Http\Controllers\EnclosController;
use App\Http\Controllers\EnvironnementController;
use App\Http\Controllers\EspeceController;
use App\Http\Controllers\PersonnelController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\TaxeController;
use App\Http\Controllers\TypeEnclosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');
//
//require __DIR__.'/auth.php';

Route::get('/admin', function () {
    return view('pages/admin/index');
})->middleware(['auth'])->name('admin.index');

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::resource('/personnels', PersonnelController::class);
    Route::resource('/enclos', EnclosController::class);
    Route::resource('/taxes', TaxeController::class);
    Route::resource('/produits', ProduitController::class);
    Route::resource('/categories-produits', CategorieProduitController::class);
    Route::resource('/dinosaures', DinosaureController::class);
    Route::resource('/especes', EspeceController::class);
    Route::resource('/caracteristiques', CaracteristiqueController::class);
    Route::resource('/climats', ClimatController::class);
    Route::resource('/environnements', EnvironnementController::class);
    Route::resource('/types-enclos', TypeEnclosController::class);
    Route::resource('/adresses', AdresseController::class);
//    Route::resource('/clients', ClientController::class);
    Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
    Route::get('/clients/{client}/edit', [ClientController::class, 'edit'])->name('clients.edit');
    Route::put('/clients/{client}', [ClientController::class, 'update'])->name('clients.update');
    Route::delete('/clients/{client}', [ClientController::class, 'destroy'])->name('clients.destroy');
});

require __DIR__.'/auth.php';

Route::get('/clients/create/{userId}', [ClientController::class, 'create'])->name('clients.create');
Route::post('/clients', [ClientController::class, 'store'])->name('clients.store');
Route::post('/clients/{client}', [ClientController::class, 'store'])->name('clients.show');

Route::post('/adresse', [AdresseController::class, 'store'])->name('adresse.store');
Route::get('/boutique', [CommandeController::class, 'index'])->name('boutique.index');
Route::post('/panier', [CommandeController::class, 'create'])->name('panier.create');
Route::put('/panier', [CommandeController::class, 'store'])->middleware(['auth'])->name('panier.store');

Route::get('/', [DinosaureController::class, 'homepage'])->name('homepage');
