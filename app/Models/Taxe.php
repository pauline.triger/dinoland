<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxe extends Model
{
    use HasFactory;

    protected $fillable = ['taux'];

    public function produits(){
        return $this->hasMany(Produit::class);
    }

    public function getTauxAsPercentageAttribute() {
        return $this->attributes['taux'] * 100;
    }
}
