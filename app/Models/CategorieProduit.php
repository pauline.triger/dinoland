<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorieProduit extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $table = "categories_produits";

    public function produits(){
        return $this->hasMany(Produit::class);
    }
}
