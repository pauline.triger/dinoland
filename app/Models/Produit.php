<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Produit extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'prix', 'quantite', 'image', 'taxe_id', 'categorie_produit_id'];

    public function commandes(){
        return $this->belongsToMany(Commande::class, 'lignes_commandes', 'produit_id', 'commande_id')
            ->withTimestamps();
    }

    public function clients(){
        return $this->belongsToMany(Client::class, 'panier', 'produit_id', 'client_id')
            ->withPivot('quantite')
            ->withTimestamps();
    }

    public function categorie() {
        return $this->belongsTo(CategorieProduit::class, 'categorie_produit_id');
    }

    public function taxe() {
        return $this->belongsTo(Taxe::class);
    }

    public function slugName() {
        $slugName = Str::slug($this->attributes['nom']) . '-'. Carbon::now()->format('Y-m-d');
        return $slugName;
    }
}
