<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adresse extends Model
{
    use HasFactory;

    protected $fillable = ['numero', 'rue', 'cp', 'ville', 'pays_id'];

    public function pays(){
        return $this->belongsTo(Pays::class);
    }

    public function clientFacturation(){
        return $this->belongsTo(Client::class);
    }

    public function clientLivraison(){
        return $this->belongsTo(Client::class);
    }

    public function personnels(){
        return $this->hasMany(Personnel::class);
    }
}
