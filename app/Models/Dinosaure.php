<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dinosaure extends Model
{
    use HasFactory;

    protected $table = "dinos";

    protected $fillable = ['nom', 'taille', 'poids', 'image', 'enclos_id', 'espece_id', 'nourriture_id'];

    public function caracteristiques(){
        return $this->belongsToMany(Caracteristique::class, 'caracteristique_dino', 'dino_id', 'caracteristique_id');
    }

    public function enclos(){
        return $this->belongsTo(Enclos::class);
    }

    public function espece(){
        return $this->belongsTo(Espece::class);
    }

    public function nourriture(){
        return $this->belongsTo(Nourriture::class);
    }

    public function computeAge(){
        $age = Carbon::parse($this->created_at)->age;
        return $age;
    }
}
