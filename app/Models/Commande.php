<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;

    protected $fillable = ['prixTTC', 'mode_livraison_id', 'client_id', 'statut_commande_id'];

    public function produits(){
        return $this->belongsToMany(Produit::class, 'ligne_commande', 'commande_id', 'produit_id')
            ->withPivot('quantite', 'prixHT', 'taux')
            ->withTimestamps();
    }

    public function statutCommande(){
        return $this->belongsTo(StatutCommande::class);
    }

    public function modeLivraison(){
        return $this->belongsTo(ModeLivraison::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }
}
