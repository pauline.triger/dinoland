<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enclos extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function dinosaures(){
        return $this->hasMany(Dinosaure::class);
    }

    public function environnements(){
        return $this->belongsToMany(Environnement::class, 'enclos_environnement', 'enclos_id', 'environnement_id')
            ->withTimestamps();
    }

    public function personnels(){
        return $this->belongsToMany(Personnel::class, 'enclos_personnel');
    }

    public function climat(){
        return $this->belongsTo(Climat::class);
    }

    public function typeEnclos(){
        return $this->belongsTo(TypeEnclos::class);
    }
}
