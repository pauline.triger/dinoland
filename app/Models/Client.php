<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'prenom', 'tel', 'adresse_livraison_id', 'adresse_facturation_id'];

    public function produits(){
        return $this->belongsToMany(Produit::class, 'panier', 'client_id', 'produit_id')
            ->withPivot('quantite')
            ->withTimestamps();
    }

    public function adresseFacturation(){
        return $this->belongsTo(Adresse::class, "adresse_facturation_id");
    }

    public function adresseLivraison(){
        return $this->belongsTo(Adresse::class, "adresse_livraison_id");
    }

    public function commandes(){
        return $this->hasMany(Commande::class);
    }

    public function user(){
        return $this->hasOne(User::class);
    }
}
