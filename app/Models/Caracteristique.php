<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Caracteristique extends Model
{
    use HasFactory;

    protected $fillable = ['nom'];

    public function dinosaures(){
        return $this->belongsToMany(Dinosaure::class, 'caracteristique_dino', 'caracteristique_id', 'dino_id');
    }
}
