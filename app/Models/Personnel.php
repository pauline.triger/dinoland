<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personnel extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'prenom', 'tel', 'email', 'type_id', 'adresse_id'];

    public function enclos(){
        return $this->belongsToMany(Enclos::class, 'enclos_personnel');
    }

    public function typePersonnel(){
        return $this->belongsTo(TypePersonnel::class, 'type_id');
    }

    public function adresse(){
        return $this->belongsTo(Adresse::class);
    }

    public function getBeautifulDateAttribute() {
        $dt = new Carbon($this->attributes['created_at']);
        return $dt->format('d/m/Y');
    }
}
