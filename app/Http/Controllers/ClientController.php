<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{

    public function index() {
        $clients = Client::all();

        $userId = Auth::check() ? Auth::id() : null;

        return view('pages/admin/client/index', [
            'clients' => $clients,
            'userId' => $userId
        ]);
    }

    public function store(Request $request) {
        $client = Client::create($request->all());
        $client->adresse_facturation_id = $request->adresse_id;
        $client->adresse_livraison_id = $request->adresse_id;
        $client->user_id = Auth::id();
        $client->save();
        return redirect()->route('boutique.index');
    }

    public function create($userId = null) {
        return view('pages/admin/client/create', [
            'userId' => $userId
        ]);
    }

    public function show(Client $client) {
        $client = Client::with(['commandes', 'produits'])
            ->select(['id', 'nom', 'prenom', 'email', 'tel', 'adresse_livraison_id', 'adresse_facturation_id','created_at'])
            ->where('id', $client->id)
            ->firstOrFail();

        return view('pages/admin/client/show', [
            "dinosaure" => $client,
        ]);
    }

    public function update(Request $request, Client $client) {
        $client->update($request->all());
        return redirect()->route('clients.index');
    }

    public function edit($clientId) {
        $client = Client::find($clientId);
        return view('pages/admin/client/edit', [
            "client" => $client,
        ]);
    }

    public function destroy(Client $client) {
        $client->delete();
        return redirect()->route('clients.index');
    }
}
