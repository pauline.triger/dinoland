<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use http\Client\Response;
use Illuminate\Http\Request;

class AdresseController extends Controller
{
    public function store(Request $request) {
        if ($request->ajax()) {
            $nouvelleAdresse = Adresse::create([
                'numero' => $request->numero,
                'rue' => $request->rue,
                'cp' => $request->cp,
                'ville' => $request->ville,
                'pays_id' => 1,
            ]);
        }

        return response()->json($nouvelleAdresse->toArray());
    }

    public function create($data) {
        $nouvelleAdresse = Adresse::create([
            'numero' => $data->numero,
            'rue' => $data->rue,
            'cp' => $data->cp,
            'ville' => $data->ville,
            'pays_id' => 1,
        ]);

        return $nouvelleAdresse;
    }

    public function show($adresseId) {
        $adresse = Adresse::find($adresseId);
        return view('pages/admin/adresse/show', ['adresse' => $adresse]);
    }

    public function edit($adresseId) {
        $adresse = Adresse::find($adresseId);
        return view('pages/admin/adresse/edit', ['adresse' => $adresse]);
    }

    public function update(Request $request, $categorieId){
        $adresse = Adresse::find($categorieId);
        $adresse->update($request->all());
        return redirect()->route('personnels.index');
    }

    public function destroy($adresseId) {
        $adresse = Adresse::find($adresseId);
        $adresse->delete();
        return redirect()->route('personnels.index');
    }
}
