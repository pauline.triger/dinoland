<?php

namespace App\Http\Controllers;

use App\Models\Climat;
use App\Models\Dinosaure;
use App\Models\Enclos;
use App\Models\Environnement;
use App\Models\Personnel;
use App\Models\TypeEnclos;
use Illuminate\Http\Request;

class EnclosController extends Controller
{
    public function index() {
        $enclosures = Enclos::with(['personnels', 'dinosaures', 'environnements', 'climat', 'typeEnclos'])
            ->select(['id', 'nom', 'superficie', 'type_enclos_id', 'climat_id'])
            ->get();

        return view('pages/admin/enclos/index', ['enclosures' => $enclosures]);
    }

    public function show($enclosId) {
        $enclosure = Enclos::with(['personnels', 'dinosaures', 'environnements', 'climat', 'typeEnclos'])
            ->select(['id', 'nom', 'superficie', 'type_enclos_id', 'climat_id'])
            ->where('id', $enclosId)
            ->firstOrFail();

        return view('pages/admin/enclos/show', [
            'enclos' => $enclosure
        ]);
    }

    public function create() {
        $dinosaures = Dinosaure::pluck('nom','id');
        $climats = Climat::pluck('nom', 'id');
        $typeEnclos = TypeEnclos::pluck('nom', 'id');
        $environnements = Environnement::all();
        $personnels = Personnel::pluck('nom', 'id');
        return view('pages/admin/enclos/create', [
            'dinosaures' => $dinosaures,
            'climats' => $climats,
            'typeEnclos' => $typeEnclos,
            'environnements' => $environnements,
            'personnels' => $personnels,
        ]);
    }

    public function store(Request $request) {
        $enclos = Enclos::create($request->all());

        if ( isset($request->all()['dinosaures'])) {
            foreach($request->all()['dinosaures'] as $i => $dinosaureId) {
                $dinosaure = Dinosaure::find($dinosaureId);
                $dinosaure->enclos_id = $enclos->id;
                $dinosaure->save();
            }
        }

        foreach($request->all()['superficie-environnement'] as $environnementId => $superficie) {
            if (isset($superficie)) {
                $enclos->environnements()->attach([
                    $environnementId => ['superficie' => $superficie],
                ]);
            }
        }


        if ( isset($request->all()['personnels'])) {
            foreach ($request->all()['personnels'] as $i => $personnelId) {
                $enclos->personnels()->attach([$personnelId]);
            }
        }

        return redirect()->route('enclos.index');
    }

    public function edit(Enclos $enclosId) {
        $enclos = Enclos::find($enclosId);
        return view('pages/admin/caracteristique/edit', [
            'caracteristique' => $enclos
        ]);
    }

    public function update(Request $request, $enclosId) {
        $enclos = Enclos::find($enclosId);
        $enclos->update($request->all());
        return redirect()->route('enclos.index');
    }

    public function destroy($enclosId) {
        $enclos = Enclos::find($enclosId);
        $enclos->delete();
        return redirect()->route('enclos.index');
    }
}
