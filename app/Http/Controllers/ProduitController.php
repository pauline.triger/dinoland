<?php

namespace App\Http\Controllers;

use App\Models\CategorieProduit;
use App\Models\Produit;
use App\Models\Taxe;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    public function index() {
        $categories = CategorieProduit::pluck('nom','id');;
        $taxes = Taxe::pluck('taux','id');;
        $produits = Produit::with(['categorie', 'taxe'])
            ->get();
        return view('pages/admin/produit/index', [
            'produits' => $produits,
            'categories' => $categories,
            'taxes' => $taxes
        ]);
    }

    public function create() {
        $categories = CategorieProduit::pluck('nom','id');
        $taxes = Taxe::pluck('taux','id');
        return view('pages/admin/produit/create', [
            'categories' => $categories,
            'taxes' => $taxes
        ]);
    }

    public function store(Request $request) {
        $produit = Produit::create($request->all());
        $imageName = $produit->slugName() . '.' . $request->file('image')->extension();

        $request->file('image')->storeAs('public', $imageName);
        $produit->image = $imageName;
        $produit->save();
        return redirect()->route('produits.index');
    }

    public function edit($produitId) {
        $produit = Produit::find($produitId);
        $taxes = Taxe::pluck('taux', 'id');
        $categories = CategorieProduit::pluck('nom', 'id');
        return view('pages/admin/produit/edit', [
            "produit" => $produit,
            "taxes" => $taxes,
            "categories" => $categories,
        ]);
    }

    public function update(Request $request, Produit $produit) {
        $produit->update($request->all());
        $imageName = $produit->slugName() . '.' . $request->file('image')->extension();

        $request->file('image')->storeAs('public', $imageName);
        $produit->image = $imageName;
        $produit->save();
        return redirect()->route('produits.index');
    }

    public function destroy(Produit $produit) {
        $produit->delete();
        return redirect()->route('produits.index');
    }
}
