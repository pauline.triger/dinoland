<?php

namespace App\Http\Controllers;

use App\Models\Caracteristique;
use App\Models\CategorieProduit;
use Illuminate\Http\Request;

class CaracteristiqueController extends Controller
{
    public function index() {
        $features = Caracteristique::all();
        return view('pages/admin/caracteristique/index', [
            'features' => $features
        ]);
    }

    public function show(Caracteristique $caracteristique) {
        $feature = Caracteristique::select(['id', 'nom'])
            ->where('id', $caracteristique->id)
            ->firstOrFail();


        return view('pages/admin/caracteristique/show', [
            'feature' => $feature
        ]);
    }

    public function create() {
        return view('pages/admin/caracteristique/create');
    }

    public function store(Request $request) {
        Caracteristique::create($request->all());
        return redirect()->route('caracteristiques.index');
    }

    public function edit($caracteristiqueId) {
        $caracteristique = Caracteristique::find($caracteristiqueId);
        return view('pages/admin/caracteristique/edit', [
            'caracteristique' => $caracteristique
        ]);
    }

    public function update(Request $request, $caracteristiqueId) {
        $caracteristique = Caracteristique::find($caracteristiqueId);
        $caracteristique->update($request->all());
        return redirect()->route('caracteristiques.index');
    }

    public function destroy($caracteristiqueId) {
        $caracteristique = Caracteristique::find($caracteristiqueId);
        $caracteristique->delete();
        return redirect()->route('caracteristiques.index');
    }
}
