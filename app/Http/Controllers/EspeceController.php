<?php

namespace App\Http\Controllers;

use App\Models\Espece;
use Illuminate\Http\Request;

class EspeceController extends Controller
{
    public function index() {
        $especes = Espece::all();
        return view('pages/admin/espece/index', ['especes' => $especes]);
    }

    public function show($especeId) {
        $espece = Espece::with(['dinosaures'])
            ->where('id', $especeId)
            ->firstOrFail();
        return view('pages/admin/espece/show', ['espece' => $espece]);
    }

    public function store(Request $request) {
        Espece::create($request->all());
        return redirect()->route('especes.index');
    }

    public function update(Request $request, Espece $espece) {
        $espece->update($request->all());
        return redirect()->route('especes.index');
    }

    public function destroy(Espece $espece) {
        $espece->delete();
        return redirect()->route('especes.index');
    }
}
