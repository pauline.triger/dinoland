<?php

namespace App\Http\Controllers;

use App\Models\Climat;
use Illuminate\Http\Request;

class ClimatController extends Controller
{
    public function index() {
        $climats = Climat::all();
        return view('pages/admin/climat/index', [
            'climats' => $climats
        ]);
    }

    public function show(Climat $climatId) {
        $climat = Climat::select(['id', 'nom'])
            ->where('id', $climatId->id)
            ->firstOrFail();

        return view('pages/admin/climat/show', [
            'climat' => $climat
        ]);
    }

    public function create() {
        return view('pages/admin/climat/create');
    }

    public function store(Request $request) {
        Climat::create($request->all());
        return redirect()->route('caracteristiques.index');
    }

    public function edit($climatId) {
        $climat = Climat::find($climatId);
        return view('pages/admin/caracteristique/edit', [
            'caracteristique' => $climat
        ]);
    }

    public function update(Request $request, $climatId) {
        $climat = Climat::find($climatId);
        $climat->update($request->all());
        return redirect()->route('caracteristiques.index');
    }

    public function destroy($climatId) {
        $climat = Climat::find($climatId);
        $climat->delete();
        return redirect()->route('caracteristiques.index');
    }
}
