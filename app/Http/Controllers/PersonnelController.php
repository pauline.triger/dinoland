<?php

namespace App\Http\Controllers;

use App\Models\Adresse;
use App\Models\Personnel;
use App\Models\TypePersonnel;
use Illuminate\Http\Request;

class PersonnelController extends Controller
{
    public function index() {
        $staff = Personnel::with(['typePersonnel', 'adresse'])
            ->select(['id','prenom', 'nom', 'type_id', 'adresse_id','email', 'tel', 'created_at'])
            ->get();

        return view('pages/admin/personnel/index', ['staff' => $staff]);
    }

    public function store(Request $request) {
        Personnel::create(array_merge($request->all(), ["type_id" => $request->poste]));
        return redirect()->route('personnels.index');
    }

    public function create() {
        $postes = TypePersonnel::pluck('nom','id');
        return view('pages/admin/personnel/create', ['postes' => $postes]);
    }

    public function show(Personnel $personnel) {
        $staffMember = Personnel::with(['typePersonnel', 'adresse', 'enclos'])
            ->select(['id', 'prenom', 'nom', 'type_id', 'adresse_id','email', 'tel', 'created_at'])
            ->where('id', $personnel->id)
            ->firstOrFail();
        return view('pages/admin/personnel/show', ["staffMember" => $staffMember]);
    }

    public function update(Request $request, Personnel $personnel) {
        $personnel->update($request->all());
        return redirect()->route('personnels.index');
    }

    public function edit(Personnel $personnel) {
        $postes = TypePersonnel::pluck('nom', 'id');
        return view('pages/admin/personnel/edit', [
            "personnel" => $personnel,
            "postes" => $postes
        ]);
    }

    public function destroy(Personnel $personnel) {
        $personnel->delete();
        return redirect()->route('personnels.index');
    }


}
