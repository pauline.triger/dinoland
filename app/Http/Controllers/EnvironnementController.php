<?php

namespace App\Http\Controllers;

use App\Models\Environnement;
use Illuminate\Http\Request;

class EnvironnementController extends Controller
{
    public function index() {
        $environments = Environnement::all();
        return view('pages/admin/environnement/index', [
            'environments' => $environments
        ]);
    }

    public function show(Environnement $environmentId) {
        $environment = Environnement::select(['id', 'nom'])
            ->where('id', $environmentId->id)
            ->firstOrFail();


        return view('pages/admin/environnement/show', [
            'environment' => $environment
        ]);
    }

    public function create() {
        return view('pages/admin/environnement/create');
    }

    public function store(Request $request) {
        Environnement::create($request->all());
        return redirect()->route('environnements.index');
    }

    public function edit($environmentId) {
        $environment = Environnement::find($environmentId);
        return view('pages/admin/environnement/edit', [
            'environment' => $environment
        ]);
    }

    public function update(Request $request, $environmentId) {
        $environment = Environnement::find($environmentId);
        $environment->update($request->all());
        return redirect()->route('environnements.index');
    }

    public function destroy($environmentId) {
        $environment = Environnement::find($environmentId);
        $environment->delete();
        return redirect()->route('environnements.index');
    }
}
