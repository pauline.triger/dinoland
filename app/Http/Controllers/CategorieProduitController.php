<?php

namespace App\Http\Controllers;

use App\Models\CategorieProduit;
use Illuminate\Http\Request;

class CategorieProduitController extends Controller
{
    public function index() {
        $categories = CategorieProduit::with(['produits'])
            ->select('id', 'nom')
            ->get();

        return view('pages/admin/categorie-produit/index', [
            'categories' => $categories
        ]);
    }

    public function show($category) {
        $categorieProduit = CategorieProduit::find($category);
        return view('pages/admin/categorie-produit/show', ["categorie" => $categorieProduit]);
    }

    public function create(){
        return view('pages/admin/categorie-produit/create');
    }

    public function store(Request $request){
        CategorieProduit::create($request->all());
        return redirect()->route('categories-produits.index');
    }

    public function edit($categorieId){
        $categorieProduit = CategorieProduit::find($categorieId);
        return view('pages/admin/categorie-produit/edit', ['categorie' => $categorieProduit]);
    }

    public function update(Request $request, $categorieId){
        $categorie = CategorieProduit::find($categorieId);
        $categorie->update($request->all());
        return redirect()->route('categories-produits.index');
    }

    public function destroy($categorieId) {
        $categorie = CategorieProduit::find($categorieId);
        $categorie->delete();
        return redirect()->route('categories-produits.index');
    }

}
