<?php

namespace App\Http\Controllers;

use App\Models\Taxe;
use Illuminate\Http\Request;

class TaxeController extends Controller
{
    public function index() {
        $taxes = Taxe::all();

        return view('pages/admin/taxe/index', ['taxes' => $taxes]);
    }

    public function store(Request $request) {
        Taxe::create($request->all());
        return redirect()->route('taxes.index');
    }

    public function update(Request $request, Taxe $taxe) {
        $taxe->update($request->all());
        return redirect()->route('taxes.index');
    }

    public function destroy(Taxe $taxe) {
        $taxe->delete();
        return redirect()->route('taxes.index');
    }
}
