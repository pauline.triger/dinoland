<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Commande;
use App\Models\ModeLivraison;
use App\Models\Produit;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommandeController extends Controller
{
    public function index() {
        $produits = Produit::with(['taxe'])
            ->select('id', 'nom', 'prix', 'image', 'taxe_id')
            ->get();

        return view('pages/boutique/index', [
            'produits' => $produits
        ]);
    }

    public function create(Request $request) {
        $userId = Auth::id();

        $client = Client::select('id', 'nom', 'prenom')
            ->where('user_id', $userId)
            ->firstOrFail();

        foreach($request->produits as $productId => $qte) {
            if ($qte > 0) {
                $produit = Produit::with(['clients', 'taxe'])
                    ->where('id', $productId)
                    ->firstOrFail();

                $produit->clients()->attach($client->id, ['quantite' => $qte]);
                $produit->save();
            }
        }


        $modeLivraison = ModeLivraison::pluck('nom', 'id');

        // On crée la commande qui sera mise à jour à la validation du panier
        $commande = Commande::create([
            'prixTTC' => 0,
            'mode_livraison_id' => 1, // valeur par défaut qui sera mise à jour à la validation du panier
            'statut_commande_id' => 1, // correspond à un statut de commande "panier", qui sera aussi mis à jour ultérieurement
            'client_id' => $client->id
        ]);



        return view('pages/boutique/panier', [
            'client' => $client,
            'modesLivraison' => $modeLivraison,
            'commande' => $commande,
        ]);
    }

    public function store(Request $request) {

        $commande = Commande::find($request->commande);
        $commande->update([
            'mode_livraison_id' => $request->all()['livraison'],
            'statut_commande_id' => 4, // statut en attente de confirmation de la commande par DinoLand
        ]);

        $userId = Auth::id();

        $clientId = Client::where('user_id', $userId)
            ->firstOrFail();

        foreach($request->produits as $produitId => $qte) {
            $produit = Produit::with(['clients'])
                ->where('id', $produitId)
                ->firstOrFail();

            $commande->produits()->attach($produitId, [
                'quantite' => $qte,
                'prixHT' => $produit->prix,
                'taux' => 0.10
            ]);

            $produit->clients()->detach($clientId, ['quantite' => $qte]);
            $produit->save();
        }

        $prixTTC = 0;

        foreach ($commande->produits as $ligneCommande ) {
            $prixTTC += $ligneCommande->pivot->prixHT * $ligneCommande->pivot->quantite + $ligneCommande->pivot->prixHT * $ligneCommande->pivot->quantite * $ligneCommande->pivot->taux;
        }
        $commande->update([
            'prixTTC' => $prixTTC
        ]);

        $commande->save();

        return view('pages/boutique/commande', [
            'commande' => $commande
        ]);
    }
}
