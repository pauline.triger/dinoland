<?php

namespace App\Http\Controllers;

use App\Models\TypeEnclos;
use Illuminate\Http\Request;

class TypeEnclosController extends Controller
{
    public function index() {
        $enclosureTypes = TypeEnclos::all();

        return view('pages/admin/type-enclos/index', [
            'enclosureTypes' => $enclosureTypes
        ]);
    }

    public function create() {

    }

    public function store() {

    }

    public function edit($typeId) {
        $enclosureType = TypeEnclos::find($typeId);
        return view('pages/admin/type-enclos/edit', [
            'type' => $enclosureType
        ]);
    }

    public function update(Request $request, $typeId) {
        $typeEnclos = TypeEnclos::find($typeId);
        $typeEnclos->update($request->all());

        return redirect()->route('types-enclos.index');
    }

    public function destroy($typeId) {
        $enclosureType = TypeEnclos::find($typeId);
        $enclosureType->delete();
        return view('pages/admin/type-enclos/index');
    }
}
