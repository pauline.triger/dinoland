<?php

namespace App\Http\Controllers;

use App\Models\Dinosaure;
use App\Models\Enclos;
use App\Models\Espece;
use App\Models\Nourriture;
use Illuminate\Http\Request;

class DinosaureController extends Controller
{
    public function homepage() {
        $dinosaures = Dinosaure::all();
        return view('homepage', [
            'dinosaures' => $dinosaures
        ]);
    }

    public function index() {
        $dinosaures = Dinosaure::with(['enclos', 'espece', 'nourriture', 'caracteristiques'])
            ->select(['id','nom', 'taille', 'poids', 'espece_id', 'enclos_id', 'nourriture_id', 'created_at'])
            ->get();

        return view('pages/admin/dinosaure/index', ['dinosaures' => $dinosaures]);
    }

    public function store(Request $request) {
        $dinosaure = Dinosaure::create(array_merge($request->all()));
        if (!empty($request->file('image'))) {
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('public', $imageName);
            $dinosaure->image = $imageName;
            $dinosaure->save();
        }
        return redirect()->route('dinosaures.index');
    }

    public function create() {
        $especes = Espece::pluck('nom', 'id');
        $enclos = Enclos::pluck('nom', 'id');
        $nourritures = Nourriture::pluck('nom', 'id');
        return view('pages/admin/dinosaure/create', [
            'enclos' => $enclos,
            'nourriture' => $nourritures,
            'especes' => $especes
        ]);
    }

    public function show(Dinosaure $dinosaure) {
        $dinosaure = Dinosaure::with(['nourriture', 'enclos', 'espece'])
            ->select(['id', 'nom', 'taille', 'image', 'poids', 'nourriture_id', 'enclos_id','espece_id', 'created_at'])
            ->where('id', $dinosaure->id)
            ->firstOrFail();

        $ageDino = $dinosaure->computeAge();

        return view('pages/admin/dinosaure/show', [
            "dinosaure" => $dinosaure,
            "age" => $ageDino
        ]);
    }

    public function update(Request $request, Dinosaure $dinosaure) {
        $dinosaure->update($request->all());
        if (!empty($request->file('image'))) {
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('public', $imageName);
            $dinosaure->image = $imageName;
            $dinosaure->save();
        }
        return redirect()->route('dinosaures.index');
    }

    public function edit($dinosaureId) {
        $dinosaure = Dinosaure::find($dinosaureId);
        $especes = Espece::pluck('nom', 'id');
        $enclos = Enclos::pluck('nom', 'id');
        $nourritures = Nourriture::pluck('nom', 'id');
        return view('pages/admin/dinosaure/edit', [
            "dinosaure" => $dinosaure,
            "especes" => $especes,
            "enclos" => $enclos,
            "nourritures" => $nourritures
        ]);
    }

    public function destroy(Dinosaure $dinosaure) {
        $dinosaure->delete();
        return redirect()->route('dinosaures.index');
    }
}
