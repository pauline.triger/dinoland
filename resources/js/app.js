require('./bootstrap');
import "bootstrap";

import $ from 'jquery';
window.$ = window.jQuery = $;

document.addEventListener('DOMContentLoaded', (e) => {
    const saveAddressBtn = $("#save-address")

    // ajout d'une adresse sur la page de création d'un nouveau membre du personnel
    $(saveAddressBtn).click(function (e) {
        e.preventDefault();
        var data = {}
        $("#new-address-form").children("input").each(function (index) {

            var name = $(this).attr("name")
            data[name] = $(this).val()
        })

        $("#new-address-form").reset

        $.ajax({
            url: "/adresse",
            method: "post",
            dataType: 'json',
            data: data,
            success: function(result){
                $("#new-user-address #numero").text(result.numero)
                $("#new-user-address #rue").text(result.rue)
                $("#new-user-address #cp").text(result.cp)
                $("#new-user-address #ville").text(result.ville)
                $("#adresse-id").val(result.id)
                $("#new-address-btn").css("display","none")
                $("#new-user-address").css("display","block")
            }
        });
    })

    // Création d'une commande
    var plusBtn = $('.fa-plus-circle')
    var minusBtn = $('.fa-minus-circle')

    var qte = $(this).siblings('input').val()

    $(plusBtn).click(function (e) {
        e.preventDefault()
        var qte = parseFloat($(this).siblings('input').val())
        $(this).siblings('input').val(qte + 1)

        computeMontantPanier()
    })

    $(minusBtn).click(function (e) {
        e.preventDefault()
        var qte = parseFloat($(this).siblings('input').val())
        if (qte > 0) {
            $(this).siblings('input').val(qte - 1)
        } else {
            alert('vous ne pouvez pas insérer de valeur inférieure à 0')
        }

        computeMontantPanier()
    })

    $("#check-cart").click(function(e) {
        e.preventDefault()

        $.ajax({

        })
    })


    const INPUT_SUPERFICIE = $('#inputs-superficie')

    // Définir les superficies des enclos
    $("#select-environments").children("select").children("option").click( function (e) {
        var nomEnvironnement = this.innerText
        var environnementId = this.value


        var input = '<input name="environnement-superficie[]" type="number" class="form-control"' +
            'placeholder='+nomEnvironnement+'>'

        var inputSuperficie =   '<div class="input-group mb-3">' +
            input +
            '<span class="input-group-text">en km²</span>' +
            '</div>'

        $(INPUT_SUPERFICIE).append(inputSuperficie)

    })

});

function computeMontantPanier() {
    var amount = 0
    var cartAmount = $("#cart-amount")
    var productList = $("#product-list")
    $(productList).children('div').each(function(index) {
        var qte = parseFloat($(this).find('.qte-product').val())
        var prixHt = parseFloat($(this).find('.prix-ht').text())

        amount += parseFloat(qte*prixHt)


    })

    amount = Math.round(amount * 100) / 100

    $(cartAmount).text(amount)
}

