@extends('layouts.website-layout')



@section('content')
    @auth()
        <div>
            Montant du panier : <span id="cart-amount">0.00</span> €
        </div>
    @elseguest()
        <div>
            Vous devez être connecté pour passer commande
        </div>
    @endauth

    {!! Form::open(["method" => "POST", "route" => ["panier.create"]]) !!}
    <div id="product-list" class="d-flex flex-row bd-highlight text-center d-flex flex-wrap">
        @foreach($produits as $produit)
            <div class="flex-fill produit-boutique" id="produit-{{$produit->id}}">
                <h2>{{ $produit->nom }}</h2>
                <img src="{{$produit->image}}" alt="{{$produit->nom}}">
                <div>Prix : <span class="prix-ht">{{ $produit->prix}}</span></div>
                <div>Taxe : <span class="taxe">{{ $produit->taxe->taux * 100}}</span> %</div>
                <div>TTC : {{ round($produit->prix * $produit->taxe->taux + $produit->prix, 2) }}</div>
                @auth()
                <div class="d-flex m-3 justify-content-evenly">
                    <i class="fas fa-minus-circle fs-3"></i>
                        <label for="{{ 'produits['.$produit->id.']' }}"></label>
                        {!! Form::number('produits['.$produit->id.']', 0, ["class" => "text-center qte-product"]) !!}
                        <i class="fas fa-plus-circle fs-3"></i>
                </div>
                @endauth
            </div>
        @endforeach
    </div>
    @auth()
    {!! Form::submit("Enregistrer", ["class" => "btn btn-success"]) !!}
    @endauth
    {!! Form::close() !!}


@endsection
