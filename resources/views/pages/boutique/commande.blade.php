@extends('layouts.website-layout')

@section('content')
    <h1>Votre commande a bien été enregistrée</h1>
    <div class="row">
        <h1 class="text-center mb-3">Panier</h1>
        <div class="col-md-8 mx-auto">
            <table class="table">
                <thead>
                <th>Produit</th>
                <th>Quantité</th>
                <th>Prix unitaire HT</th>
                <th>Prix TTC</th>
                </thead>
                <tbody>
                @foreach($commande->produits as $produit)
                    <tr>
                        <td>{{$produit->nom}}</td>
                        <td>
                            {{$produit->pivot->quantite}}
                        </td>
                        <td>{{ $produit->pivot->prixHT}}</td>
                        <td>{{ $produit->pivot->prixHT * $produit->pivot->quantite * (1+$produit->pivot->taux)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                <p>
                    Mode de livraison : {{ $commande->modeLivraison->nom }}
                </p>
                <p>
                    Statut de la commande : {{ $commande->statutCommande->nom }}
                </p>
            </div>
        </div>
    </div>
@endsection
