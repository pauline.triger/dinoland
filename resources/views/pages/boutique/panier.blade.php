@extends('layouts.website-layout')

@section('content')
    <div class="row">
        <h1 class="text-center mb-3">Panier</h1>
        <div class="col-md-8 mx-auto">
            {!! Form::open(["method" => "PUT", "route" => ["panier.store"]]) !!}
            <table class="table">
                <thead>
                <th>Produit</th>
                <th>Quantité</th>
                <th>Prix HT</th>
                <th>Prix TTC</th>
                </thead>
                <tbody>
                @foreach($client->produits as $produitPanier)
                    {!! Form::hidden('produits['.$produitPanier->id.']', $produitPanier->pivot->quantite) !!}
                    <tr>
                        <td>{{ $produitPanier->nom}}</td>
                        <td>
                            {{$produitPanier->pivot->quantite}}
                        </td>
                        <td>{{ $produitPanier->prix}}</td>
                        <td>{{ $produitPanier->prix * $produitPanier->pivot->quantite}} + (taxes)</td>
                    </tr>
                    {!! Form::hidden('commande', $commande->id) !!}

                @endforeach
                </tbody>
            </table>
            <label for="livraison">Choix du mode de retrait</label>
            {!! Form::select('livraison', $modesLivraison, ["class" => "text-center"]) !!}

            {{ Form::button('Valider la commande', ['type' => 'submit', 'class' => 'btn btn-success'] )  }}

            {!! Form::close() !!}
        </div>
    </div>

@endsection
