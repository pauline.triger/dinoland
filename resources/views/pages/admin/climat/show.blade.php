@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $climat->id }}</td>
            <td>{{ $climat->nom}}</td>
            <td>
                <td><a href="{{ route('climats.edit', ["climat" => $climat]) }}"><i class="fas fa-edit"></i></a></td>
            </td>
            <td>
                {!! Form::open(["method" => "DELETE", "route" => ["climats.destroy", "climat" => $climat]]) !!}
                {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                {!! Form::close() !!}
            </td>
        </tr>
        </tbody>
    </table>
@endsection
