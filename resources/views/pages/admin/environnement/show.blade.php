@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Nom</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $environment->id }}</td>
            <td>{{ $environment->nom}}</td>
            <td>
                <a href="{{ route('environnements.edit', ["environnement" => $environment]) }}">Modifier la caractéristique</a>
            </td>
            <td>
                {!! Form::open(["method" => "DELETE", "route" => ["environnements.destroy", "environnement" => $environment]]) !!}
                {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                {!! Form::close() !!}
            </td>
        </tr>
        </tbody>
    </table>
@endsection
