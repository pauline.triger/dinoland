@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Nom</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($environments as $environment)
            <tr>
                <td>{{$environment->id}}</td>
                <td>{{$environment->nom}}</td>
                <td><a href="{{ route('environnements.show', $environment->id) }}" class="btn btn-secondary">Voir l'environnement</a></td>
                <td><a href="{{ route('environnements.edit', $environment->id) }}" class="btn btn-warning">Modifier l'environnement</a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["environnements.destroy", [$environment->id]]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('environnements.create') }}" class="btn btn-primary">Ajouter un environnement</a>

@endsection
