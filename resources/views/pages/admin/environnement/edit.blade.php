@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div class="card mx-auto" style="width: 18rem;">
                <div class="card-body text-center">
                    <h5 class="card-title">Modifier</h5>
                    {{ Form::model($environment, ['method' => 'PUT', 'route' => ['environnements.update', [$environment->id]]]) }}
                    <label for="nom">Nom de l'environnement</label>
                    {{ Form::text("nom", $environment->nom, ["class" => "form-group form-control"]) }}

                    <div class="d-flex flex-row bd-highlight justify-content-center">
                        {{ Form::submit("Mettre à jour", ["class" => "btn btn-primary m-2"]) }}
                        {{ Form::close() }}

                        {!! Form::open(["method" => "DELETE", "route" => ["environnements.destroy", [$environment->id]]]) !!}
                        {!! Form::submit("Supprimer", ["class" => "btn btn-danger m-2"]) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
