@extends('layouts.admin-layout')

@section('content')
<table class="table">
    <thead>
    <tr>
        <th scope="col">Nom</th>
        <th scope="col">Prix</th>
        <th scope="col">Quantité</th>
        <th scope="col">Taxe</th>
        <th scope="col">Catégorie</th>
    </tr>
    </thead>
    <tbody>
    @foreach($produits as $produit)
    <tr>
        <td>{{$produit->nom}}</td>
        <td>{{$produit->prix}}</td>
        <td>{{$produit->quantite}}</td>
        <td>{{$produit->taxe->taux_as_percentage}} %</td>
        <td><a href="{{ route('categories-produits.show', $produit->categorie->id) }}">{{$produit->categorie->nom}}</a></td>
        <td>
        <td><a href="{{ route('produits.show',$produit) }}"><i class="fas fa-eye"></i></a></td>
        <td><a href="{{ route('produits.edit',$produit) }}"><i class="fas fa-edit"></i></a></td>
        <td>
            {!! Form::open(["method" => "DELETE", "route" => ["produits.destroy", "produit" => $produit]]) !!}
            {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'    ] )  }}

            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<a href="{{route('produits.create')}}" class="btn btn-success">
    Ajouter un produit
</a>

@endsection
