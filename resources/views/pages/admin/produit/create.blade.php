@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
            {!! Form::open(["method" => "POST", "route" => "produits.store", 'files' => true]) !!}
            <label for="nom">Nom du produit</label>
            {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
            <label for="prix">Prix HT</label>
            {!! Form::number("prix", null, ["class" => "form-group form-control", "step" => "0.01"]) !!}
            <label for="quantite">Quantité en stock</label>
            {!! Form::number("quantite", null, ["class" => "form-group form-control"]) !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="categorie_produit_id" class="input-group-text text-dark">Catégorie</label>
                </div>
                {!! Form::select("categorie_produit_id", $categories, ["class" => "custom-select"]) !!}
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="taxe_id" class="input-group-text text-dark">Taxe</label>
                </div>
                {!! Form::select("taxe_id", $taxes, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <label for="image" class="form-label">Image : </label>
                    {!! Form::file('image', ["class" => "form-control"]) !!}
            </div>

            {!! Form::submit("Ajouter le produit", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="new-address" tabindex="-1" aria-labelledby="modal-new-address" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une adresse</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {!! Form::open(["method" => "POST", "route" => ["adresse.store"], "id" => "new-address-form"] ) !!}
                    <label for="numero">Numéro</label>
                    {!! Form::number("numero", null, ["class" => "form-group form-control"]) !!}
                    <label for="rue">Rue</label>
                    {!! Form::text("rue", null, ["class" => "form-group form-control"]) !!}
                    <label for="cp">Code Postal</label>
                    {!! Form::text("cp", null, ["class" => "form-group form-control"]) !!}
                    <label for="ville">Ville</label>
                    {!! Form::text("ville", null, ["class" => "form-group form-control"]) !!}

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="button" id="save-address" class="btn btn-primary" data-bs-dismiss="modal">Enregistrer l'adresse</button>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection
