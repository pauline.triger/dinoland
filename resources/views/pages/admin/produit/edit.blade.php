@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            Photo
        </div>
        <div class="col-md-8">
            {!! Form::model($produit, ["method" => "PUT", "route" => ["produits.update", $produit], 'files' => true]) !!}
            <label for="nom">Nom</label>
            {!! Form::text("nom", $produit->nom, ["class" => "form-group form-control"]) !!}
            <label for="quantite">Quantité en stock</label>
            {!! Form::number("quantite", $produit->quantite, ["class" => "form-group form-control", "step" => 1]) !!}
            <label for="prix">Prix HT</label>
            {!! Form::number("prix", $produit->prix, ["class" => "form-group form-control", "step" => 1]) !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="taxe_id" class="input-group-text text-dark">Taxe</label>
                </div>
                {!! Form::select("taxe_id", $taxes, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="categorie_id" class="input-group-text text-dark">Catégorie</label>
                </div>
                {!! Form::select("categorie_id", $categories, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <label for="image" class="form-label">Image : </label>
                {!! Form::file('image', ["class" => "form-control"]) !!}
            </div>
            {!! Form::submit("Mettre à jour", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
