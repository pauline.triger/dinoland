@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div class="card mx-auto" style="width: 18rem;">
                <div class="card-body text-center">
                    <h5 class="card-title">Modifier</h5>
                    {{ Form::model($categorie, ['method' => 'PUT', 'route' => ['categories-produits.update', [$categorie->id]]]) }}
                    <label for="nom">Nom du type de l'enclos</label>
                    {{ Form::text("nom", $categorie->nom, ["class" => "form-group form-control"]) }}

                    <div class="d-flex flex-row bd-highlight justify-content-center">
                        {{ Form::submit("Mettre à jour", ["class" => "btn btn-primary m-2"]) }}
                        {{ Form::close() }}

                        {!! Form::open(["method" => "DELETE", "route" => ["categories-produits.destroy", [$categorie->id]]]) !!}
                        {!! Form::submit("Supprimer", ["class" => "btn btn-danger m-2"]) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
