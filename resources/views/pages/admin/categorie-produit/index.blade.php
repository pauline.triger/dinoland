@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        </thead>
        <tbody>
        @foreach($categories as $categorie)
            <tr>
                <td>{{$categorie->id}}</td>
                <td>{{$categorie->nom}}</td>
                <td><a href="{{ route('categories-produits.show', ["categories_produit" => $categorie]) }}"><i class="fas fa-eye"></i></a></td>
                <td><a href="{{ route('categories-produits.edit', ["categories_produit" => $categorie]) }}"><i class="fas fa-edit"></i></a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["categories-produits.destroy", "categories_produit" => $categorie]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="{{ route('categories-produits.create') }}" class="btn btn-primary">Ajouter une catégorie de produit</a>
@endsection
