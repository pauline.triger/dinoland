@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-8 text-left">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">Nom de la catégorie</th>

                    <td>{{$categorie->nom}}</td>
                </tr>
                <tr>
                    <th scope="row">Produits de la catégorie : {{$categorie->nom}}</th>
                    <td>
                        @foreach($categorie->produits as $produit)
                            <span class="mx-1">{{$produit->nom}}</span><br>
                        @endforeach
                    </td>
                </tr>

                </tbody>
            </table>

            <div class="d-flex">
                <div class="btn btn-warning mx-2">
                    <a href="{{ route('categories-produits.index') }}">Voir la liste des catégories de produit </a>
                </div>
                <div class="mx-2">
                    {!! Form::open(["method" => "DELETE", "route" => ["categories-produits.destroy", $categorie]]) !!}
                    {!! Form::submit("Supprimer", ["class" => "btn btn-danger"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
