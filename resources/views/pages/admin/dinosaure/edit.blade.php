@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            Photo
        </div>
        <div class="col-md-8">
            {!! Form::model($dinosaure, ["method" => "PUT", "route" => ["dinosaures.update", $dinosaure], 'files' => true]) !!}
            <label for="nom">Nom</label>
            {!! Form::text("nom", $dinosaure->nom, ["class" => "form-group form-control"]) !!}
            <label for="taille">Taille (en m)</label>
            {!! Form::number("taille", $dinosaure->taille, ["class" => "form-group form-control", "step" => 0.1]) !!}
            <label for="poids">Poids (en tonnes)</label>
            {!! Form::number("poids", $dinosaure->poids, ["class" => "form-group form-control"]) !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="enclos" class="input-group-text text-dark">Enclos</label>
                </div>
                {!! Form::select("enclos", $enclos, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="especes" class="input-group-text text-dark">Espèce</label>
                </div>
                {!! Form::select("especes", $especes, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="nourriture" class="input-group-text text-dark">Nourriture</label>
                </div>
                {!! Form::select("nourriture", $nourritures, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <label for="image" class="form-label">Image : </label>
                {!! Form::file('image', ["class" => "form-control"]) !!}
            </div>
            {!! Form::submit("Mettre à jour", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
