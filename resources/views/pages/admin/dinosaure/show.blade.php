@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4 text-center">
            <div>
                <img src="{{ asset('storage/'.$dinosaure->image) }}" class="img-fluid" alt="dino-img">
            </div>
            <div>{{ $dinosaure->nom }}</div>
            <br>
        </div>
        <div class="col-md-8 text-left">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">Date de naissance</th>
                    <td>{{$dinosaure->created_at}}</td>
                </tr>
                <tr>
                    <th scope="row">Taille</th>
                    <td>{{$dinosaure->taille}} m</td>
                </tr>
                <tr>
                    <th scope="row">Poids</th>
                    <td>{{$dinosaure->poids}}</td>
                </tr>
                <tr>
                    <th scope="row">Caractéristiques</th>
                    <td>
                        @foreach($dinosaure->caracteristiques as $caracteristique)
                            <span class="mx-1">
                                {{$caracteristique->nom}}
                            </span>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <th scope="row">Espèce</th>
                    <td>
                        {{$dinosaure->espece->nom }}
                    </td>
                </tr>
                <tr>
                    <th scope="row">Enclos</th>
                    <td>
                        {{$dinosaure->enclos->nom }}
                    </td>
                </tr>
                <tr>
                    <th scope="row">Régime alimentaire</th>
                    <td>
                        {{$dinosaure->nourriture->nom }}
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="d-flex">
                <div class="btn btn-warning mx-2">
                    <a href="{{ route('dinosaures.edit', ['dinosaure' => $dinosaure]) }}">Modifier les informations</a>
                </div>
                <div class="mx-2">
                    {!! Form::open(["method" => "DELETE", "route" => ["dinosaures.destroy", ['dinosaure' => $dinosaure]]]) !!}
                    {!! Form::submit("Supprimer", ["class" => "btn btn-danger"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
