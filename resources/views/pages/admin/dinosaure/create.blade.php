@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-7 mx-auto">
            {!! Form::open(["method" => "POST", "route" => "dinosaures.store", 'files' => true]) !!}
            <label for="nom">Nom du dinosaure</label>
            {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
            <label for="taille">Taille</label>
            {!! Form::number("taille", null, ["class" => "form-group form-control", "step" => "0.01"]) !!}
            <label for="poids">Poids</label>
            {!! Form::number("poids", null, ["class" => "form-group form-control"]) !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="enclos_id" class="input-group-text text-dark">Enclos</label>
                </div>
                {!! Form::select("enclos_id", $enclos, ["class" => "custom-select"]) !!}
            </div>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="espece_id" class="input-group-text text-dark">Espèce</label>
                </div>
                {!! Form::select("espece_id", $especes, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="nourriture_id" class="input-group-text text-dark">Nourriture</label>
                </div>
                {!! Form::select("nourriture_id", $nourriture, ["class" => "custom-select"]) !!}
            </div>

            <div class="input-group mb-3">
                <label for="image" class="form-label">Image : </label>
                {!! Form::file('image', ["class" => "form-control"]) !!}
            </div>

            {!! Form::submit("Ajouter le produit", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
