@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
            <th scope="col">Nom</th>
            <th scope="col">Taille (en m)</th>
            <th scope="col">Poids (en tonnes)</th>
            <th scope="col">Caractéristique</th>
            <th scope="col">Espèce</th>
            <th scope="col">Enclos</th>
            <th scope="col">Régime alimentaire</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>
        <tbody>
        @foreach($dinosaures as $dinosaure)
            <tr>
                <td>{{$dinosaure->nom}}</td>
                <td>{{$dinosaure->taille}}</td>
                <td>{{$dinosaure->poids}}</td>
                <td>
                    @foreach($dinosaure->caracteristiques as $caracteristique)
                        {{$caracteristique->nom}}
                    @endforeach
                </td>
                <td><a href="{{ route('especes.show', $dinosaure->espece->id) }}">{{$dinosaure->espece->nom}}</a></td>
                <td><a href="{{ route('enclos.show', $dinosaure->enclos->id) }}">{{$dinosaure->enclos->nom}}</a></td>
                <td>{{$dinosaure->nourriture->nom}}</td>
                <td><a href="{{ route('dinosaures.show', ["dinosaure" => $dinosaure]) }}"><i class="fas fa-eye"></i></a></td>
                <td><a href="{{ route('dinosaures.edit', ["dinosaure" => $dinosaure]) }}"><i class="fas fa-edit"></i></a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["dinosaures.destroy", "dinosaure" => $dinosaure]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('dinosaures.create') }}" class="btn btn-primary">Ajouter un dinosaure</a>
@endsection
