@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($enclosureTypes as $type)
            <tr>
                <td>{{$type->id}}</td>
                <td>{{$type->nom}}</td>
                <td><a href="{{ route('types-enclos.edit', $type->id) }}" class="btn btn-warning">Modifier le type de l'enclos</a></td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
