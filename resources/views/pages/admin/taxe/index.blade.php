@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Taux</th>
        </tr>
        </thead>
        <tbody>
        @foreach($taxes as $taxe)
        <tr>
            <th scope="row">{{$taxe->id}}</th>
            <td>{{$taxe->taux}}</td>
            <td>
                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#edit-tax">
                    Modifier la taxe
                </button>

                <div class="modal fade" id="edit-tax" tabindex="-1" aria-labelledby="modal-edit-tax" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modifer le taux</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(["method" => "PUT", "route" => ["taxes.update", "tax" => $taxe]]) !!}
                                <label for="taux">Taux</label>
                                {!! Form::number("taux", $taxe->taux, ["class" => "form-group form-control", "step" => 0.01]) !!}
                                {!! Form::submit("Modifier", ["class" => "btn btn-success"]) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>

            <td>
                {!! Form::open(["method" => "DELETE", "route" => ["taxes.destroy", "tax" => $taxe]]) !!}
                {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#new-tax">
        Ajouter une taxe
    </button>


    <div class="modal fade" id="new-tax" tabindex="-1" aria-labelledby="modal-new-tax" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une adresse</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {!! Form::open(["method" => "POST", "route" => "taxes.store"]) !!}
                    <label for="taux">Taux</label>
                    {!! Form::number("taux", null, ["class" => "form-group form-control", "step" => 0.01]) !!}
                    {!! Form::submit("Ajouter", ["class" => "btn btn-success"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection
