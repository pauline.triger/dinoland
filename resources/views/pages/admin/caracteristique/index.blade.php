@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
        <th scope="col">#</th>
        <th scope="col">Nom</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        </thead>
        <tbody>
        @foreach($features as $feature)
            <tr>
                <td>{{$feature->id}}</td>
                <td>{{$feature->nom}}</td>
                <td><a href="{{ route('caracteristiques.show', ["caracteristique" => $feature]) }}"><i class="fas fa-eye"></i></a></td>
                <td><a href="{{ route('caracteristiques.edit', ["caracteristique" => $feature]) }}"><i class="fas fa-edit"></i></a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["caracteristiques.destroy", "caracteristique" => $feature->id]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="{{ route('caracteristiques.create') }}" class="btn btn-primary">Ajouter une caractéristique</a>
@endsection

