@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $feature->id }}</td>
                <td>{{ $feature->nom}}</td>
                <td>
                <td><a href="{{ route('caracteristiques.edit', ["caracteristique" => $feature]) }}"><i class="fas fa-edit"></i></a></td>
                </td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["caracteristiques.destroy", "caracteristique" => $feature]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                    {!! Form::close() !!}
                </td>
            </tr>
        </tbody>
    </table>
@endsection
