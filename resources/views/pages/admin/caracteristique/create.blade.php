@extends('layouts.admin-layout')

@section('content')
    {!! Form::open(["method" => "POST", "route" => "caracteristiques.store"]) !!}
    <label for="nom">Caractéristique</label>
    {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
    {!! Form::submit("Ajouter", ["class" => "btn btn-success"]) !!}
    {!! Form::close() !!}
@endsection
