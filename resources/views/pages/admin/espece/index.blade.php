@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Taux</th>
        </tr>
        </thead>
        <tbody>
        @foreach($especes as $espece)
        <tr>
            <th scope="row">{{$espece->id}}</th>
            <td>{{$espece->nom}}</td>
            <td>
                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#edit-specie">
                    Modifier l'espèce
                </button>

                <div class="modal fade" id="edit-specie" tabindex="-1" aria-labelledby="modal-edit-specie" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modifer l'espèce</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(["method" => "PUT", "route" => ["especes.update", "espece" => $espece]]) !!}
                                <label for="nom">Espèce</label>
                                {!! Form::text("nom", $espece->nom, ["class" => "form-group form-control"]) !!}
                                {!! Form::submit("Modifier", ["class" => "btn btn-success"]) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>

            <td>
                {!! Form::open(["method" => "DELETE", "route" => ["especes.destroy", "espece" => $espece]]) !!}
                {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#new-specie">
        Ajouter une espèce
    </button>


    <div class="modal fade" id="new-specie" tabindex="-1" aria-labelledby="modal-new-specie" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une espèce</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {!! Form::open(["method" => "POST", "route" => "especes.store"]) !!}
                    <label for="nom">Espèce</label>
                    {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
                    {!! Form::submit("Ajouter", ["class" => "btn btn-success"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection
