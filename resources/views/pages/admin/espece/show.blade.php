@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-8 text-left">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">Nom de l'espèce</th>
                    <td>{{$espece->nom}}</td>
                </tr>
                <tr>
                    <th scope="row">{{$espece->nom}}s du parc</th>
                    <td>
                        @foreach($espece->dinosaures as $dinosaures)
                            <span class="mx-1">{{$dinosaures->nom}}</span>
                        @endforeach
                    </td>
                </tr>

                </tbody>
            </table>

            <div class="d-flex">
                <div class="btn btn-warning mx-2">
                    <a href="{{ route('especes.index', $espece) }}">Voir les espèces</a>
                </div>
                <div class="mx-2">
                    {!! Form::open(["method" => "DELETE", "route" => ["especes.destroy", $espece]]) !!}
                    {!! Form::submit("Supprimer", ["class" => "btn btn-danger"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
