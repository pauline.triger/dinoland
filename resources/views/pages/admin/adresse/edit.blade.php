@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div class="card mx-auto" style="width: 18rem;">
                <h5 class="card-title">Modifier</h5>
                    {{ Form::model($adresse, ['method' => 'PUT', 'route' => ['adresses.update', [$adresse->id]]]) }}
                <label for="numero">Rue</label>
                    {{ Form::number("numero", $adresse->numro, ["class" => "form-group form-control"]) }}
                <label for="rue">Rue</label>
                {{ Form::text("rue", $adresse->rue, ["class" => "form-group form-control"]) }}
                <label for="cp">Code postal</label>
                    {{ Form::text("cp", $adresse->cp, ["class" => "form-group form-control"]) }}
                <label for="ville">Ville</label>
                    {{ Form::text("ville", $adresse->ville, ["class" => "form-group form-control"]) }}
                <div class="d-flex flex-row bd-highlight justify-content-center">
                    {{ Form::submit("Mettre à jour", ["class" => "btn btn-primary m-2"]) }}
                    {{ Form::close() }}

                    {!! Form::open(["method" => "DELETE", "route" => ["adresses.destroy", [$adresse->id]]]) !!}
                    {!! Form::submit("Supprimer", ["class" => "btn btn-danger m-2"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
