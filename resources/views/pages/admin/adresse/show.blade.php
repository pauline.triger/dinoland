@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-8 text-left">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th scope="row">Adresse</th>
                    <td>
                        {{$adresse->numero . ' ' . $adresse->rue}} <br>
                        {{$adresse->cp . ' ' . $adresse->ville}}
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="d-flex">
                <div class="btn btn-warning mx-2">
                    <a href="{{ route('adresses.edit', $adresse->id) }}">Mdofiier l'adresse</a>
                </div>
            </div>
        </div>
    </div>
@endsection
