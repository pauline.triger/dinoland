@extends('layouts.admin-layout')

@section('content')
    <div class="alert alert-success" style="display:none"></div>
    <div class="row">
        <div class="col-md-4">
            Photo
        </div>
        <div class="col-md-8">
            {!! Form::open(["method" => "POST", "route" => ["personnels.store"]]) !!}
            <label for="prenom">Prénom</label>
            {!! Form::text("prenom", null, ["class" => "form-group form-control"]) !!}
            <label for="nom">Nom</label>
            {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
            <label for="tel">Numéro de téléphone</label>
            {!! Form::text("tel", null, ["class" => "form-group form-control"]) !!}
            <label for="email">Adresse e-mail</label>
            {!! Form::email("email", null, ["class" => "form-group form-control"]) !!}

            <div>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#new-address" id="new-address-btn">
                    Ajouter une adresse
                </button>
                <div id="new-user-address">
                    <p><span id="numero"></span> <span id="rue"></span></p>
                    <p><span id="cp"></span> <span id="ville"></span></p>
                </div>
                <input type="hidden" name="adresse_id" id="adresse-id" value="">
            </div>




            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="poste" class="input-group-text text-dark">Poste</label>
                </div>
                {!! Form::select("poste", $postes, ["class" => "custom-select"]) !!}
            </div>
            {!! Form::submit("Enregistrer", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="new-address" tabindex="-1" aria-labelledby="modal-new-address" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajouter une adresse</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {!! Form::open(["method" => "POST", "route" => ["adresse.store"], "id" => "new-address-form"] ) !!}
                    <label for="numero">Numéro</label>
                    {!! Form::number("numero", null, ["class" => "form-group form-control"]) !!}
                    <label for="rue">Rue</label>
                    {!! Form::text("rue", null, ["class" => "form-group form-control"]) !!}
                    <label for="cp">Code Postal</label>
                    {!! Form::text("cp", null, ["class" => "form-group form-control"]) !!}
                    <label for="ville">Ville</label>
                    {!! Form::text("ville", null, ["class" => "form-group form-control"]) !!}

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                        <button type="button" id="save-address" class="btn btn-primary" data-bs-dismiss="modal">Enregistrer l'adresse</button>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection
