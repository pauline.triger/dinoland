@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4 text-center">
            <div>
                <img src="https://via.placeholder.com/200" alt="profile-picture">
            </div>
            <div>{{ $staffMember->prenom . ' ' . $staffMember->nom }}</div>
            <br>
            <div>{{ $staffMember->tel }}</div>
            <div>{{ $staffMember->email }}</div>
        </div>
        <div class="col-md-8 text-left">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th scope="row">Date d'embauche</th>
                        <td>{{$staffMember->beautiful_date}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Poste</th>
                        <td>{{$staffMember->typePersonnel->nom}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Enclos actuel</th>
                    <td>
                        @foreach($staffMember->enclos as $affiliatedEnclosure)
                        <span class="mx-1">
                            {{$affiliatedEnclosure->nom}}
                        </span>
                        @endforeach
                    </td>
                    </tr>
                    <tr>
                        <th scope="row">Adresse</th>
                        <td>
                            {{$staffMember->adresse->numero . ' ' . $staffMember->adresse->rue }}
                            <br>
                            {{$staffMember->adresse->cp . ' ' . $staffMember->adresse->ville}}
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="d-flex">
                <div class="btn btn-warning mx-2">
                    <a href="{{ route('personnels.edit', ['personnel' => $staffMember]) }}">Modifier les informations</a>
                </div>
                <div class="mx-2">
                    {!! Form::open(["method" => "DELETE", "route" => ["personnels.destroy", ['personnel' => $staffMember]]]) !!}
                    {!! Form::submit("Supprimer", ["class" => "btn btn-danger"]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
