@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
            <th scope="col">Membre</th>
            <th scope="col">Poste</th>
            <th scope="col">Coordonnées</th>
            <th scope="col">Adresse</th>
            <th scope="col">Date d'ajout</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
        </thead>
        <tbody>
            @foreach($staff as $staffMember)
                <tr>
                    <td>{{$staffMember->prenom . ' ' . $staffMember->nom}}</td>
                    <td>{{$staffMember->typePersonnel->nom}}</td>
                    <td>
                        {{$staffMember->email}} <br>
                        {{$staffMember->tel}}
                    </td>
                    <td>
                        <a href="{{ route('adresses.show', $staffMember->id) }}"><i class="fas fa-house-user"></i></a>
                    </td>
                    <td>{{$staffMember->beautiful_date}}</td>
                    <td><a href="{{ route('personnels.show', ["personnel" => $staffMember]) }}"><i class="fas fa-eye"></i></a></td>
                    <td><a href="{{ route('personnels.edit', ["personnel" => $staffMember]) }}"><i class="fas fa-user-edit"></i></a></td>
                    <td>
                        {!! Form::open(["method" => "DELETE", "route" => ["personnels.destroy", "personnel" => $staffMember]]) !!}
                        {{ Form::button('<i class="fas fa-user-minus"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <a href="{{ route('personnels.create') }}" class="btn btn-primary">Ajouter un nouvel employé</a>
@endsection
