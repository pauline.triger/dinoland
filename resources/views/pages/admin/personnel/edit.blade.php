@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            Photo
        </div>
        <div class="col-md-8">
            {!! Form::model($personnel, ["method" => "PUT", "route" => ["personnels.update", "personnel" => $personnel]]) !!}
            <label for="prenom">Prénom</label>
            {!! Form::text("prenom", $personnel->prenom, ["class" => "form-group form-control"]) !!}
            <label for="nom">Nom</label>
            {!! Form::text("nom", $personnel->nom, ["class" => "form-group form-control"]) !!}
            <label for="tel">Numéro de téléphone</label>
            {!! Form::text("tel", $personnel->tel, ["class" => "form-group form-control"]) !!}
            <label for="email">Adresse e-mail</label>
            {!! Form::email("email", $personnel->email, ["class" => "form-group form-control"]) !!}
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="poste" class="input-group-text text-dark">Poste</label>
                </div>
                {!! Form::select("poste", $postes, ["class" => "custom-select"]) !!}
            </div>
            {!! Form::submit("Mettre à jour", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
