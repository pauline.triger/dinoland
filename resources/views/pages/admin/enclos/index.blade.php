@extends('layouts.admin-layout')

@section('content')
<table class="table">
    <thead>
        <tr>
            <th>Nom</th>
            <th>Superficie</th>
            <th>Dinosaures</th>
            <th>Employés</th>
            <th>Environnements</th>
            <th>Climat</th>
            <th>Type Enclos</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($enclosures as $enclosure)
        <tr>
            <td>{{$enclosure->nom}}</td>
            <td>{{$enclosure->superficie}} km²</td>
            <td>
                @foreach($enclosure->dinosaures as $dinosaure)
                    <a href="{{ route('dinosaures.show', [$dinosaure->id]) }}">{{ $dinosaure->nom }}</a> <br>
                @endforeach
            </td>
            <td>
                @foreach($enclosure->personnels as $employe)
                    <a href="{{ route('personnels.show', [$employe->id]) }}">{{ $employe->prenom.' '.$employe->nom }} <br></a>
                @endforeach
            </td>
            <td>
                @foreach($enclosure->environnements as $environnement)
                    {{ $environnement->nom }} <br>
                @endforeach
            </td>
            <td>{{$enclosure->climat->nom}}</td>
            <td>{{$enclosure->typeEnclos->nom}}</td>
            <td><a href="{{ route('enclos.show', ["enclo" => $enclosure->id]) }}"><i class="fas fa-eye"></i></a></td>
            <td><a href="{{ route('enclos.edit', $enclosure->id) }}"><i class="fas fa-edit"></i></a></td>
            <td>
                {!! Form::open(["method" => "DELETE", "route" => ["enclos.destroy", "enclo" => $enclosure->id]]) !!}
                {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<a href="{{ route('enclos.create') }}" class="btn btn-primary">Ajouter un enclos</a>
@endsection
