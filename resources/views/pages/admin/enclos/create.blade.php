@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col">
            {!! Form::open(["method" => "POST", "route" => 'enclos.store']) !!}
            <label for="nom">Nom de l'enclos</label>
            {!! Form::text("nom", null, ["class" => "form-group form-control"]) !!}
        </div>
        <div>
            <label for="superficie">Superficie de l'enclos</label>
            {!! Form::number("superficie", null, ["class" => "form-group form-control", "step" => 1]) !!}
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="climat_id" class="input-group-text text-dark">Climat</label>
                </div>
                {!! Form::select("climat_id", $climats, ["class" => "custom-select"]) !!}
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label for="type_enclos_id" class="input-group-text text-dark">Type d'enclos</label>
                </div>
                {!! Form::select("type_enclos_id", $typeEnclos, ["class" => "custom-select"]) !!}
            </div>
        </div>
    </div>
<div class="row">
    <div class="col">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label for="dinosaures[]" class="input-group-text text-dark">Dinosaures</label>
            </div>
            {!! Form::select('dinosaures[]', $dinosaures, ["class" => "custom-select"], ['multiple' => 'multiple']) !!}
        </div>
    </div>
    <div class="col">
        <div class="input-group">
            <div class="input-group-prepend h-100">
                <label for="personnels[]" class="input-group-text text-dark">Membres du personnel</label>
            </div>
            <div class="custom-select">
                {!! Form::select('personnels[]', $personnels, null, ["class" => "custom-select",'multiple']) !!}
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col">
            @foreach($environnements as $environnement)

            <div class="input-group mb-3" id="select-environments">

                {{ $environnement->nom .' : '}} {!!  Form::number('superficie-environnement['.$environnement->id.']', null, ["class" => "custom-select", "multiple"]) !!}

            </div>
            @endforeach
        </div>
    </div>

<br>
{!! Form::submit("Ajouter", ["class" => "btn btn-success"]) !!}
{!! Form::close() !!}
@endsection
