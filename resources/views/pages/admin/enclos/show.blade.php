@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
            <tr>
                <th>Nom</th>
                <th>Superficie</th>
                <th>Dinosaures</th>
                <th>Employés</th>
                <th>Environnements</th>
                <th>Climat</th>
                <th>Type Enclos</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$enclos->nom}}</td>
                <td>{{$enclos->superficie}} km²</td>
                <td>
                    @foreach($enclos->dinosaures as $dinosaure)
                        <a href="{{ route('dinosaures.show', [$dinosaure->id]) }}">{{$dinosaure->nom}}</a><br>
                    @endforeach
                </td>
                <td>
                    @foreach($enclos->personnels as $employe)
                        <a href="{{ route('personnels.show', [$employe->id]) }}">{{$employe->prenom . ' '. $employe->nom}}</a><br>
                    @endforeach
                </td>
                <td>
                    @foreach($enclos->environnements as $environnement)
                        {{ $environnement->nom}}<br>
                    @endforeach
                </td>
                <td>{{$enclos->climat->nom}}</td>
                <td>{{$enclos->typeEnclos->nom}}</td>
                <td><a href="{{ route('enclos.edit', ["enclo" => $enclos->id]) }}"><i class="fas fa-edit"></i></a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["enclos.destroy", "enclo" => $enclos->id]]) !!}
                    {{ Form::button('<i class="fas fa-trash"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                    {!! Form::close() !!}
                </td>
            </tr>
        </tbody>
    </table>

    <a href="{{ route('enclos.create') }}" class="btn btn-primary">Ajouter un enclos</a>
@endsection

