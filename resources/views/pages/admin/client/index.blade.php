@extends('layouts.admin-layout')

@section('content')
    <table class="table">
        <thead class="table-dark">
        <th scope="col">Prénom et nom</th>
        <th scope="col">Coordonnées</th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        </thead>
        <tbody>
        @foreach($clients as $client)
            <tr>
                <td>{{$client->prenom . ' ' . $client->nom}}</td>
                <td>
                    {{$client->users->email}} <br>
                    {{$client->tel}}
                </td>
                <td><a href="{{ route('clients.show', ["client" => $client]) }}"><i class="fas fa-eye"></i></a></td>
                <td><a href="{{ route('clients.edit', ["client" => $client]) }}"><i class="fas fa-user-edit"></i></a></td>
                <td>
                    {!! Form::open(["method" => "DELETE", "route" => ["clients.destroy", "client" => $client]]) !!}
                    {{ Form::button('<i class="fas fa-user-minus"></i>', ['type' => 'submit', 'class' => 'btn'] )  }}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <a href="{{ route('clients.create', $userId) }}" class="btn btn-primary">Ajouter un client</a>
@endsection
