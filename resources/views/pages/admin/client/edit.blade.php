@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-md-4">
            Photo
        </div>
        <div class="col-md-8">
            {!! Form::model($client, ["method" => "PUT", "route" => ["clients.update", $client]]) !!}
            <label for="nom">Nom</label>
            {!! Form::text("nom", $client->nom, ["class" => "form-group form-control"]) !!}
            <label for="prenom">Prénom</label>
            {!! Form::text("prenom", $client->prenom, ["class" => "form-group form-control", "step" => 0.1]) !!}
            <label for="tel">Téléphone</label>
            {!! Form::text("tel", $client->tel, ["class" => "form-group form-control"]) !!}
            <label for="email">Email</label>
            {!! Form::text("email", $client->email, ["class" => "form-group form-control"]) !!}

            {!! Form::submit("Mettre à jour", ["class" => "btn btn-success"]) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
