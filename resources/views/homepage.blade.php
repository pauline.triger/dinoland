@extends('layouts.website-layout')

@section('content')
    <h1>Bienvenue à DinoLand</h1>
    <br>
    <h2>Nos dinosaures</h2>

    <div id="product-list" class="d-flex flex-row bd-highlight text-center d-flex flex-wrap">
        @foreach($dinosaures as $dinosaure)
            <div class="flex-fill produit-boutique" id="dinosaure-{{$dinosaure->id}}">
                <h2>{{ $dinosaure->nom }}</h2>
                <img src="{{$dinosaure->image}}" alt="{{$dinosaure->nom}}">
            </div>
        @endforeach
    </div>
@endsection()
