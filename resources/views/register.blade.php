@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-6 mx-auto">
            <div class="card text-center">
                <div class="card-header">
                    Formulaire d'inscription client
                </div>
                <div class="card-body">
                    {!! Form::open(["method" => "POST", "route" => 'login']) !!}
                    <div class="d-flex">
                        <label for="nom">Nom</label>
                        {!! Form::text('nom', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    <div class="d-flex">
                        <label for="prenom">Prénom</label>
                        {!! Form::text('prenom', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    <div class="d-flex">
                        <label for="tel">Téléphone</label>
                        {!! Form::text('tel', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    <div class="d-flex">
                        <label for="email">Adresse e-mail</label>
                        {!! Form::email('email', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    <div class="d-flex mx-auto">
                        <label for="password">Password</label>
                        {!! Form::password('password', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    {!! Form::submit('S\'inscrire', ["class" => "btn btn-primary"]) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>

@endsection
