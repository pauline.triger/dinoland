@auth()
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('homepage') }}">DinoLand</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('admin.index') }}">Tableau de bord</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ route('personnels.index') }}" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Personnel
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('personnels.index') }}">Membres du personnel</a></li>
                        <li><a class="dropdown-item" href="{{ route('personnels.create') }}">Ajouter un membre</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ route('personnels.index') }}" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dinosaures
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('dinosaures.index') }}">Dinosaures</a></li>
                        <li><a class="dropdown-item" href="{{ route('dinosaures.create') }}">Ajouter un dinosaure</a></li>
                        <li><a class="dropdown-item" href="{{ route('especes.index') }}">Gérer les espèces</a></li>
                        <li><a class="dropdown-item" href="{{ route('caracteristiques.index') }}">Gérer les caractéristiques</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Produits
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('produits.index') }}">Produits</a></li>
                        <li><a class="dropdown-item" href="{{ route('produits.create') }}">Ajouter un produit</a></li>
                        <li><a class="dropdown-item" href="{{ route('categories-produits.index') }}">Gérer les catégories</a></li>
                        <li><a class="dropdown-item" href="{{ route('taxes.index') }}">Gérer les taxes</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Enclos
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('enclos.index') }}">Enclos</a></li>
                        <li><a class="dropdown-item" href="{{ route('climats.index') }}">Gérer les climats</a></li>
                        <li><a class="dropdown-item" href="{{ route('environnements.index') }}">Gérer les environnements</a></li>
                        <li><a class="dropdown-item" href="{{ route('types-enclos.index') }}">Gérer les types d'enclos</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Clients
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('clients.index') }}">Clients</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
@endauth
