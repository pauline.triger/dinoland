@extends('layouts.admin-layout')

@section('content')
    <div class="row">
        <div class="col-6 mx-auto">
            <div class="card text-center">
                <div class="card-header">
                    Formulaire de connexion
                </div>
                <div class="card-body">
                    {!! Form::open(["method" => "POST", "route" => 'login']) !!}
                    <div class="d-flex">
                        <label for="email">Adresse Email</label>
                        {!! Form::text('email', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    <div class="d-flex">
                        <label for="password">Password</label>
                        {!! Form::text('password', null, ["class" => "form-group form-control"]) !!}
                    </div>

                    {!! Form::submit('S\'inscrire', ["class" => "btn btn-primary"]) !!}
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>

@endsection
